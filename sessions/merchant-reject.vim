" ~/.vim/sessions/merchant-reject.vim:
" Vim session script.
" Created by session.vim 2.4.8 on 14 October 2013 at 09:40:08.
" Open this file in Vim and run :source % to restore your session.

set guioptions=egmrL
silent! set guifont=
if exists('g:syntax_on') != 1 | syntax on | endif
if exists('g:did_load_filetypes') != 1 | filetype on | endif
if exists('g:did_load_ftplugin') != 0 | filetype plugin off | endif
if exists('g:did_indent_on') != 0 | filetype indent off | endif
if &background != 'dark'
	set background=dark
endif
if !exists('g:colors_name') || g:colors_name != 'desert' | colorscheme desert | endif
call setqflist([])
let SessionLoad = 1
if &cp | set nocp | endif
let s:so_save = &so | let s:siso_save = &siso | set so=0 siso=0
let v:this_session=expand("<sfile>:p")
silent only
cd ~/
if expand('%') == '' && !&modified && line('$') <= 1 && getline(1) == ''
  let s:wipebuf = bufnr('%')
endif
set shortmess=aoO
badd +50 /Applications/MacVim.app/Contents/Resources/vim/vimrc
badd +20 .vim/UltiSnips/php.snippets
badd +60 /Volumes/vhosts/axia-db/app/View/MerchantRejects/index.ctp
badd +204 /Volumes/vhosts/axia-db/app/Controller/MerchantRejectsController.php
badd +266 /Volumes/vhosts/axia-db/app/Model/MerchantReject.php
badd +38 /Volumes/vhosts/axia-db/app/Controller/AppController.php
badd +3 .vim/UltiSnips/ctp.snippets
badd +25 .vim/UltiSnips/html.snippets
badd +17 /Volumes/vhosts/axia-db/app/Plugin/Filter/View/Elements/filter_form_fields.ctp
badd +38 /Volumes/vhosts/axia-db/app/Plugin/Filter/Controller/Component/FilterComponent.php
badd +1 /Volumes/vhosts/axia-db/app/Plugin/Filter/Model/Behavior/FilteredBehavior.php
badd +1 /Volumes/vhosts/axia-db/app/Plugin/Filter/Model/FilterAppModel.php
badd +37 /Volumes/vhosts/axia-db/app/View/MerchantRejects/edit.ctp
badd +1 /Volumes/vhosts/axia-db/app/Plugin/Filter/Controller/FilterAppController.php
badd +45 /Volumes/vhosts/axia-db/app/Model/AppModel.php
badd +18 /Volumes/vhosts/axia-db/app/Controller/Component/JediComponent.php
badd +1 /Volumes/vhosts/axia-db/app/Controller/Component/AccessControlComponent.php
badd +1 /Volumes/vhosts/axia-db/app/Controller/Component/JediComponent
badd +1 /Volumes/vhosts/axia-db/app/Controller/Component/FlagStatusLogicComponent.php
silent! argdel *
edit /Volumes/vhosts/axia-db/app/Plugin/Filter/Controller/Component/FilterComponent.php
set splitbelow splitright
wincmd _ | wincmd |
vsplit
wincmd _ | wincmd |
vsplit
2wincmd h
wincmd w
wincmd w
set nosplitbelow
set nosplitright
wincmd t
set winheight=1 winwidth=1
exe 'vert 1resize ' . ((&columns * 28 + 134) / 269)
exe 'vert 2resize ' . ((&columns * 120 + 134) / 269)
exe 'vert 3resize ' . ((&columns * 119 + 134) / 269)
" argglobal
enew
" file NERD_tree_2
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal nofen
wincmd w
" argglobal
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 196 - ((0 * winheight(0) + 37) / 75)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
196
normal! 0
wincmd w
" argglobal
edit /Volumes/vhosts/axia-db/app/Plugin/Filter/Model/Behavior/FilteredBehavior.php
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 91 - ((74 * winheight(0) + 37) / 75)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
91
normal! 024|
wincmd w
exe 'vert 1resize ' . ((&columns * 28 + 134) / 269)
exe 'vert 2resize ' . ((&columns * 120 + 134) / 269)
exe 'vert 3resize ' . ((&columns * 119 + 134) / 269)
tabedit /Volumes/vhosts/axia-db/app/Model/MerchantReject.php
set splitbelow splitright
wincmd _ | wincmd |
vsplit
1wincmd h
wincmd w
set nosplitbelow
set nosplitright
wincmd t
set winheight=1 winwidth=1
exe 'vert 1resize ' . ((&columns * 28 + 134) / 269)
exe 'vert 2resize ' . ((&columns * 240 + 134) / 269)
" argglobal
enew
" file NERD_tree_2
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal nofen
wincmd w
" argglobal
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 160 - ((0 * winheight(0) + 37) / 75)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
160
normal! 025|
wincmd w
2wincmd w
exe 'vert 1resize ' . ((&columns * 28 + 134) / 269)
exe 'vert 2resize ' . ((&columns * 240 + 134) / 269)
tabedit /Volumes/vhosts/axia-db/app/View/MerchantRejects/edit.ctp
set splitbelow splitright
wincmd _ | wincmd |
vsplit
1wincmd h
wincmd w
wincmd _ | wincmd |
split
1wincmd k
wincmd w
set nosplitbelow
set nosplitright
wincmd t
set winheight=1 winwidth=1
exe 'vert 1resize ' . ((&columns * 28 + 134) / 269)
exe '2resize ' . ((&lines * 41 + 38) / 77)
exe 'vert 2resize ' . ((&columns * 240 + 134) / 269)
exe '3resize ' . ((&lines * 33 + 38) / 77)
exe 'vert 3resize ' . ((&columns * 240 + 134) / 269)
" argglobal
enew
" file NERD_tree_2
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal nofen
wincmd w
" argglobal
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 1 - ((0 * winheight(0) + 20) / 41)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
1
normal! 0
wincmd w
" argglobal
edit /Volumes/vhosts/axia-db/app/View/MerchantRejects/index.ctp
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 55 - ((3 * winheight(0) + 16) / 33)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
55
normal! 072|
wincmd w
2wincmd w
exe 'vert 1resize ' . ((&columns * 28 + 134) / 269)
exe '2resize ' . ((&lines * 41 + 38) / 77)
exe 'vert 2resize ' . ((&columns * 240 + 134) / 269)
exe '3resize ' . ((&lines * 33 + 38) / 77)
exe 'vert 3resize ' . ((&columns * 240 + 134) / 269)
tabedit /Volumes/vhosts/axia-db/app/Controller/MerchantRejectsController.php
set splitbelow splitright
wincmd _ | wincmd |
vsplit
1wincmd h
wincmd w
set nosplitbelow
set nosplitright
wincmd t
set winheight=1 winwidth=1
exe 'vert 1resize ' . ((&columns * 28 + 134) / 269)
exe 'vert 2resize ' . ((&columns * 240 + 134) / 269)
" argglobal
enew
" file NERD_tree_2
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal nofen
wincmd w
" argglobal
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 57 - ((0 * winheight(0) + 37) / 75)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
57
normal! 051|
wincmd w
2wincmd w
exe 'vert 1resize ' . ((&columns * 28 + 134) / 269)
exe 'vert 2resize ' . ((&columns * 240 + 134) / 269)
tabedit /Volumes/vhosts/axia-db/app/Controller/Component/JediComponent.php
set splitbelow splitright
wincmd _ | wincmd |
vsplit
1wincmd h
wincmd w
set nosplitbelow
set nosplitright
wincmd t
set winheight=1 winwidth=1
exe 'vert 1resize ' . ((&columns * 28 + 134) / 269)
exe 'vert 2resize ' . ((&columns * 240 + 134) / 269)
" argglobal
enew
" file NERD_tree_2
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal nofen
wincmd w
" argglobal
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 10 - ((9 * winheight(0) + 37) / 75)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
10
normal! 027|
wincmd w
2wincmd w
exe 'vert 1resize ' . ((&columns * 28 + 134) / 269)
exe 'vert 2resize ' . ((&columns * 240 + 134) / 269)
tabedit .vim/UltiSnips/php.snippets
set splitbelow splitright
wincmd _ | wincmd |
vsplit
wincmd _ | wincmd |
vsplit
2wincmd h
wincmd w
wincmd w
set nosplitbelow
set nosplitright
wincmd t
set winheight=1 winwidth=1
exe 'vert 1resize ' . ((&columns * 28 + 134) / 269)
exe 'vert 2resize ' . ((&columns * 120 + 134) / 269)
exe 'vert 3resize ' . ((&columns * 119 + 134) / 269)
" argglobal
enew
" file NERD_tree_2
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal nofen
wincmd w
" argglobal
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 20 - ((19 * winheight(0) + 37) / 75)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
20
normal! 032|
wincmd w
" argglobal
edit .vim/UltiSnips/html.snippets
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 20 - ((19 * winheight(0) + 37) / 75)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
20
normal! 030|
wincmd w
2wincmd w
exe 'vert 1resize ' . ((&columns * 28 + 134) / 269)
exe 'vert 2resize ' . ((&columns * 120 + 134) / 269)
exe 'vert 3resize ' . ((&columns * 119 + 134) / 269)
tabedit /Applications/MacVim.app/Contents/Resources/vim/vimrc
set splitbelow splitright
wincmd _ | wincmd |
vsplit
1wincmd h
wincmd w
set nosplitbelow
set nosplitright
wincmd t
set winheight=1 winwidth=1
exe 'vert 1resize ' . ((&columns * 28 + 134) / 269)
exe 'vert 2resize ' . ((&columns * 240 + 134) / 269)
" argglobal
enew
" file NERD_tree_2
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal nofen
wincmd w
" argglobal
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 187 - ((74 * winheight(0) + 37) / 75)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
187
normal! 033|
wincmd w
2wincmd w
exe 'vert 1resize ' . ((&columns * 28 + 134) / 269)
exe 'vert 2resize ' . ((&columns * 240 + 134) / 269)
tabnext 2
if exists('s:wipebuf')
"   silent exe 'bwipe ' . s:wipebuf
endif
" unlet! s:wipebuf
set winheight=1 winwidth=20 shortmess=filnxtToO
let s:sx = expand("<sfile>:p:r")."x.vim"
if file_readable(s:sx)
  exe "source " . fnameescape(s:sx)
endif
let &so = s:so_save | let &siso = s:siso_save

" Support for special windows like quick-fix and plug-in windows.
" Everything down here is generated by vim-session (not supported
" by :mksession out of the box).

tabnext 1
1wincmd w
let s:bufnr_save = bufnr("%")
let s:cwd_save = getcwd()
NERDTree ~/.vim/UltiSnips
if !getbufvar(s:bufnr_save, '&modified')
  let s:wipebuflines = getbufline(s:bufnr_save, 1, '$')
  if len(s:wipebuflines) <= 1 && empty(get(s:wipebuflines, 0, ''))
    silent execute 'bwipeout' s:bufnr_save
  endif
endif
execute "cd" fnameescape(s:cwd_save)
1resize 75|vert 1resize 28|2resize 75|vert 2resize 120|3resize 75|vert 3resize 119|
tabnext 2
1wincmd w
let s:bufnr_save = bufnr("%")
let s:cwd_save = getcwd()
NERDTreeMirror
if !getbufvar(s:bufnr_save, '&modified')
  let s:wipebuflines = getbufline(s:bufnr_save, 1, '$')
  if len(s:wipebuflines) <= 1 && empty(get(s:wipebuflines, 0, ''))
    silent execute 'bwipeout' s:bufnr_save
  endif
endif
execute "cd" fnameescape(s:cwd_save)
1resize 75|vert 1resize 28|2resize 75|vert 2resize 240|
tabnext 3
1wincmd w
let s:bufnr_save = bufnr("%")
let s:cwd_save = getcwd()
NERDTreeMirror
if !getbufvar(s:bufnr_save, '&modified')
  let s:wipebuflines = getbufline(s:bufnr_save, 1, '$')
  if len(s:wipebuflines) <= 1 && empty(get(s:wipebuflines, 0, ''))
    silent execute 'bwipeout' s:bufnr_save
  endif
endif
execute "cd" fnameescape(s:cwd_save)
1resize 75|vert 1resize 28|2resize 41|vert 2resize 240|3resize 33|vert 3resize 240|
tabnext 4
1wincmd w
let s:bufnr_save = bufnr("%")
let s:cwd_save = getcwd()
NERDTreeMirror
if !getbufvar(s:bufnr_save, '&modified')
  let s:wipebuflines = getbufline(s:bufnr_save, 1, '$')
  if len(s:wipebuflines) <= 1 && empty(get(s:wipebuflines, 0, ''))
    silent execute 'bwipeout' s:bufnr_save
  endif
endif
execute "cd" fnameescape(s:cwd_save)
1resize 75|vert 1resize 28|2resize 75|vert 2resize 240|
tabnext 5
1wincmd w
let s:bufnr_save = bufnr("%")
let s:cwd_save = getcwd()
NERDTreeMirror
if !getbufvar(s:bufnr_save, '&modified')
  let s:wipebuflines = getbufline(s:bufnr_save, 1, '$')
  if len(s:wipebuflines) <= 1 && empty(get(s:wipebuflines, 0, ''))
    silent execute 'bwipeout' s:bufnr_save
  endif
endif
execute "cd" fnameescape(s:cwd_save)
1resize 75|vert 1resize 28|2resize 75|vert 2resize 240|
tabnext 6
1wincmd w
let s:bufnr_save = bufnr("%")
let s:cwd_save = getcwd()
NERDTreeMirror
if !getbufvar(s:bufnr_save, '&modified')
  let s:wipebuflines = getbufline(s:bufnr_save, 1, '$')
  if len(s:wipebuflines) <= 1 && empty(get(s:wipebuflines, 0, ''))
    silent execute 'bwipeout' s:bufnr_save
  endif
endif
execute "cd" fnameescape(s:cwd_save)
1resize 75|vert 1resize 28|2resize 75|vert 2resize 120|3resize 75|vert 3resize 119|
tabnext 7
1wincmd w
let s:bufnr_save = bufnr("%")
let s:cwd_save = getcwd()
NERDTreeMirror
if !getbufvar(s:bufnr_save, '&modified')
  let s:wipebuflines = getbufline(s:bufnr_save, 1, '$')
  if len(s:wipebuflines) <= 1 && empty(get(s:wipebuflines, 0, ''))
    silent execute 'bwipeout' s:bufnr_save
  endif
endif
execute "cd" fnameescape(s:cwd_save)
1resize 75|vert 1resize 28|2resize 75|vert 2resize 240|
tabnext 2
2wincmd w
if exists('s:wipebuf')
  if empty(bufname(s:wipebuf))
if !getbufvar(s:wipebuf, '&modified')
  let s:wipebuflines = getbufline(s:wipebuf, 1, '$')
  if len(s:wipebuflines) <= 1 && empty(get(s:wipebuflines, 0, ''))
    silent execute 'bwipeout' s:wipebuf
  endif
endif
  endif
endif
doautoall SessionLoadPost
unlet SessionLoad
" vim: ft=vim ro nowrap smc=128
