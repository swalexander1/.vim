" ~/.vim/sessions/pyro.vim:
" Vim session script.
" Created by session.vim 2.4.8 on 04 October 2013 at 08:44:03.
" Open this file in Vim and run :source % to restore your session.

set guioptions=egmrL
silent! set guifont=
if exists('g:syntax_on') != 1 | syntax on | endif
if exists('g:did_load_filetypes') != 1 | filetype on | endif
if exists('g:did_load_ftplugin') != 0 | filetype plugin off | endif
if exists('g:did_indent_on') != 0 | filetype indent off | endif
if &background != 'dark'
	set background=dark
endif
if !exists('g:colors_name') || g:colors_name != 'evening' | colorscheme evening | endif
call setqflist([])
let SessionLoad = 1
if &cp | set nocp | endif
let s:so_save = &so | let s:siso_save = &siso | set so=0 siso=0
let v:this_session=expand("<sfile>:p")
silent only
cd ~/
if expand('%') == '' && !&modified && line('$') <= 1 && getline(1) == ''
  let s:wipebuf = bufnr('%')
endif
set shortmess=aoO
badd +20 /Applications/MacVim.app/Contents/Resources/vim/vimrc
badd +6 .vim/UltiSnips/php.snippets
badd +92 Sites/pyro-axia/assets/page_types/default/merchants_list/merchants_list.css
badd +35 Sites/pyro-axia/assets/page_types/default/merchants_list/merchants_list.html
badd +12 .vim/UltiSnips/html.snippets
badd +38 Sites/pyro-axia/system/cms/modules/streams_core/field_types/relationship/field.relationship.php
badd +1 Sites/pyro-axia/system/cms/modules/streams_core/field_types/relationship/views/index.html
badd +32 Sites/pyro-axia/system/cms/modules/streams_core/field_types/relationship/views/autocomplete_js.php
badd +35 Sites/pyro-axia/addons/shared_addons/field_types/big_relationship/field.big_relationship.php
badd +33 Sites/pyro-axia/addons/shared_addons/field_types/big_relationship/views/autocomplete_js.php
badd +1 Sites/pyro-axia/addons/shared_addons/field_types/big_relationship/field.relationship.php
badd +5 Sites/pyro-axia/addons/shared_addons/field_types/big_relationship/language/english/big_relationship_lang.php
badd +1 Sites/pyro-axia/addons/shared_addons/field_types/big_relationship/language/english/relationship_lang.php
badd +38 Sites/pyro-axia/system/cms/modules/users/controllers/admin.php
badd +43 Sites/pyro-axia/system/cms/modules/users/controllers/users.php
badd +0 Sites/pyro-axia/system/cms/modules/users/views/admin/index.php
badd +0 Sites/pyro-axia/system/cms/modules/users/views/profile/view.php
silent! argdel *
edit Sites/pyro-axia/system/cms/modules/users/views/profile/view.php
set splitbelow splitright
wincmd _ | wincmd |
vsplit
1wincmd h
wincmd w
wincmd _ | wincmd |
split
1wincmd k
wincmd _ | wincmd |
vsplit
1wincmd h
wincmd w
wincmd w
wincmd t
set winheight=1 winwidth=1
exe 'vert 1resize ' . ((&columns * 34 + 134) / 269)
exe '2resize ' . ((&lines * 56 + 38) / 77)
exe 'vert 2resize ' . ((&columns * 113 + 134) / 269)
exe '3resize ' . ((&lines * 56 + 38) / 77)
exe 'vert 3resize ' . ((&columns * 120 + 134) / 269)
exe '4resize ' . ((&lines * 18 + 38) / 77)
exe 'vert 4resize ' . ((&columns * 234 + 134) / 269)
" argglobal
enew
" file NERD_tree_2
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal nofen
wincmd w
" argglobal
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 26 - ((25 * winheight(0) + 28) / 56)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
26
normal! 06|
wincmd w
" argglobal
edit Sites/pyro-axia/system/cms/modules/users/views/admin/index.php
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 22 - ((21 * winheight(0) + 28) / 56)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
22
normal! 010|
wincmd w
" argglobal
edit .vim/UltiSnips/php.snippets
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 27 - ((12 * winheight(0) + 9) / 18)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
27
normal! 028|
wincmd w
3wincmd w
exe 'vert 1resize ' . ((&columns * 34 + 134) / 269)
exe '2resize ' . ((&lines * 56 + 38) / 77)
exe 'vert 2resize ' . ((&columns * 113 + 134) / 269)
exe '3resize ' . ((&lines * 56 + 38) / 77)
exe 'vert 3resize ' . ((&columns * 120 + 134) / 269)
exe '4resize ' . ((&lines * 18 + 38) / 77)
exe 'vert 4resize ' . ((&columns * 234 + 134) / 269)
tabedit /Applications/MacVim.app/Contents/Resources/vim/vimrc
set splitbelow splitright
wincmd _ | wincmd |
vsplit
1wincmd h
wincmd w
wincmd t
set winheight=1 winwidth=1
exe 'vert 1resize ' . ((&columns * 25 + 134) / 269)
exe 'vert 2resize ' . ((&columns * 243 + 134) / 269)
" argglobal
enew
" file NERD_tree_2
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal nofen
wincmd w
" argglobal
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 161 - ((18 * winheight(0) + 37) / 75)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
161
normal! 09|
wincmd w
3wincmd w
exe 'vert 1resize ' . ((&columns * 25 + 134) / 269)
exe 'vert 2resize ' . ((&columns * 243 + 134) / 269)
tabnext 1
if exists('s:wipebuf')
"   silent exe 'bwipe ' . s:wipebuf
endif
" unlet! s:wipebuf
set winheight=1 winwidth=20 shortmess=filnxtToO
let s:sx = expand("<sfile>:p:r")."x.vim"
if file_readable(s:sx)
  exe "source " . fnameescape(s:sx)
endif
let &so = s:so_save | let &siso = s:siso_save

" Support for special windows like quick-fix and plug-in windows.
" Everything down here is generated by vim-session (not supported
" by :mksession out of the box).

tabnext 1
1wincmd w
let s:bufnr_save = bufnr("%")
let s:cwd_save = getcwd()
NERDTree ~/Sites/pyro-axia/system/cms/modules
if !getbufvar(s:bufnr_save, '&modified')
  let s:wipebuflines = getbufline(s:bufnr_save, 1, '$')
  if len(s:wipebuflines) <= 1 && empty(get(s:wipebuflines, 0, ''))
    silent execute 'bwipeout' s:bufnr_save
  endif
endif
execute "cd" fnameescape(s:cwd_save)
1resize 75|vert 1resize 34|2resize 56|vert 2resize 113|3resize 56|vert 3resize 120|4resize 18|vert 4resize 234|
tabnext 2
1wincmd w
let s:bufnr_save = bufnr("%")
let s:cwd_save = getcwd()
NERDTreeMirror
if !getbufvar(s:bufnr_save, '&modified')
  let s:wipebuflines = getbufline(s:bufnr_save, 1, '$')
  if len(s:wipebuflines) <= 1 && empty(get(s:wipebuflines, 0, ''))
    silent execute 'bwipeout' s:bufnr_save
  endif
endif
execute "cd" fnameescape(s:cwd_save)
1resize 75|vert 1resize 34|2resize 75|vert 2resize 234|
tabnext 1
3wincmd w
if exists('s:wipebuf')
  if empty(bufname(s:wipebuf))
if !getbufvar(s:wipebuf, '&modified')
  let s:wipebuflines = getbufline(s:wipebuf, 1, '$')
  if len(s:wipebuflines) <= 1 && empty(get(s:wipebuflines, 0, ''))
    silent execute 'bwipeout' s:wipebuf
  endif
endif
  endif
endif
doautoall SessionLoadPost
unlet SessionLoad
" vim: ft=vim ro nowrap smc=128
